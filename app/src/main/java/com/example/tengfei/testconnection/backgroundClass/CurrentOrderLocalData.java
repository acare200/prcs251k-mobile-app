package com.example.tengfei.testconnection.backgroundClass;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Tengfei on 14/04/2017.
 */

public class CurrentOrderLocalData {
    private static final String detail = "deliData";
    private SharedPreferences currentData;

    public CurrentOrderLocalData(Context context) {
        currentData = context.getSharedPreferences(detail, 0);
    }

    public void setDetail(DeliDetail deliDetail) {
        SharedPreferences.Editor dataEditor = currentData.edit();
        dataEditor.putInt("did", deliDetail.getDeliID());
        dataEditor.putInt("id", deliDetail.getOrderID());
        dataEditor.putInt("price", deliDetail.getOrderPrice());
        dataEditor.putString("postcode", deliDetail.getDeliPostCode());
        dataEditor.putInt("verificationCode", deliDetail.getVerificationCode());
        dataEditor.putString("address", deliDetail.getDellAddress());
        dataEditor.putString("name", deliDetail.getDelName());
        dataEditor.putInt("phoneNo", deliDetail.getDelPhoneNo());
        dataEditor.putInt("distance", deliDetail.getDistance());
        dataEditor.apply();
    }

    public DeliDetail getData() {
        int deliID = currentData.getInt("did", 0);
        int orderID = currentData.getInt("id", 0);
        int orderPrice = currentData.getInt("price", 0);
        String deliPc = currentData.getString("postcode", "");
        int verificationCode = currentData.getInt("verificationCode", 0);
        String dellAddress = currentData.getString("address", "");
        String delName = currentData.getString("name", "");
        int delPhoneNo = currentData.getInt("phoneNo", 0);
        int distance = currentData.getInt("distance", 0);
        return new DeliDetail(deliID, orderID, dellAddress, deliPc, delName, verificationCode, orderPrice, distance, delPhoneNo);
    }

    public void clearData() {
        SharedPreferences.Editor dataEditor = currentData.edit();
        dataEditor.clear();

    }
}

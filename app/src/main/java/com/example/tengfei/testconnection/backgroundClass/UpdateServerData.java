package com.example.tengfei.testconnection.backgroundClass;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.example.tengfei.testconnection.activity.Current_Order;
import com.example.tengfei.testconnection.activity.MainMenu;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Objects;

/**
 * Created by Tengfei on 04/05/2017.
 */

public class UpdateServerData extends AsyncTask<String, String, String> {
    GlobalData globalData;
    LocalData localData;
    String dta = null;
    String dataChange = null;
    private Context context;


    public UpdateServerData(Context context) {
        this.context = context;
        this.localData = new LocalData(context);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String s) {
        switch (s) {
            case "sTrue":
                Toast.makeText(context, dta + "Password Changed", Toast.LENGTH_SHORT).show();
                break;
            case "sFalse":
                Toast.makeText(context, "Failed, please check your internet.", Toast.LENGTH_SHORT).show();
                break;
            case "dTrue":
               switch (dataChange)
               {
                   case "READY":
                       Toast.makeText(context,dataChange,Toast.LENGTH_LONG).show();
                       localData.clearCurrentData();
                       Intent intent = new Intent(context, MainMenu.class);
                       ((Activity) context).finish();
                       context.startActivity(intent);

                       break;
                   case "ACCEPTED":
                       Toast.makeText(context, dataChange, Toast.LENGTH_SHORT).show();
                       Intent intent1 = new Intent(context, Current_Order.class);
                       ((Activity) context).finish();
                       context.startActivity(intent1);

                       break;
                   case "COLLECTED":
                       Toast.makeText(context, dataChange, Toast.LENGTH_SHORT).show();
                       break;
                   case "DELIVERED":
                       Toast.makeText(context, dataChange, Toast.LENGTH_SHORT).show();
                       localData.clearCurrentData();
                       localData.setcurrentDetail(null);
                       Intent intent3 = new Intent(context, MainMenu.class);
                       ((Activity) context).finish();
                       context.startActivity(intent3);

                       break;
               }

                break;
            case "dFalse":
                Toast.makeText(context, dta + "Failed, please check your internet.", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    protected String doInBackground(String... params) {
        String type;
        globalData = new GlobalData();
        String dburl = globalData.getUrl();

        int id;

        if (params != null) {
            type = params[0];
            if (type.equals(localData.getUserData().getType())) {
                id = localData.getUserData().getId();
                if (params[1] != null) {
                    dataChange = params[1];
                    Boolean check = startConnection(dburl, type, String.valueOf(id), dataChange);
                    if (check) {
                        return "sTrue";
                    } else {
                        return "sFalse";
                    }
                }

            } else if (type.contains("delivery")) {
                id = localData.getcurrentData().getOrderID();
                if (params[1] != null) {
                    dataChange = params[1];
                    Boolean check = startConnection(dburl, type, String.valueOf(id), dataChange);
                    if (check) {
                        return "dTrue";
                    } else {
                        return "dFalse";
                    }
                }
            }
        }
        return null;
    }

    @NonNull
    private Boolean startConnection(String dburl, String type, String id, String dataChange) {

        HttpURLConnection conn = null;
        try {
            URL url = new URL(dburl + type + "/" + id);
            conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(10000);
            conn.setReadTimeout(10000);
            conn.setRequestMethod("PUT");
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            OutputStreamWriter osw = new OutputStreamWriter(conn.getOutputStream());
            String dataS = setJSONdata(dataChange, type);
            osw.write(dataS);
            osw.flush();
            osw.close();
            int i = conn.getResponseCode();
            dta = conn.getResponseMessage() + String.valueOf(i);
            conn.disconnect();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            int i;
            try {
                if (conn != null) {
                    i = conn.getResponseCode();
                    dta = conn.getResponseMessage() + String.valueOf(i);
                    conn.disconnect();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        return false;
    }

    private String setJSONdata(String dataChange, String type) throws JSONException {
        String dataS = "";
        JSONObject data = new JSONObject();
        if (type.equals(localData.getUserData().getType())) {
            data.put("EMAIL_USERNAME", localData.getUserData().getEmail());
            data.put("PASSWORD", dataChange);
            data.put("SALARY_RATE", localData.getUserData().getSalaryRate());
            data.put("STAFF_BANK_ACCOUNT", localData.getUserData().getBankAcc());
            data.put("STAFF_CONTACT_NO", localData.getUserData().getPhoneNo());
            data.put("STAFF_ID", localData.getUserData().getId());
            data.put("STAFF_NAME",localData.getUserData().getFirstName());
            data.put("STAFF_SURNAME", localData.getUserData().getSurName());
            data.put("STAFF_TYPE", localData.getUserData().getType());
            dataS = data.toString();
        } else if (type.equals("delivery")) {
            data.put("ADDRESS", localData.getcurrentData().getDellAddress());
            data.put("CUSTOMER_NAME", localData.getcurrentData().getDelName());
            data.put("DELIVERY_ID", localData.getcurrentData().getDeliID());
            data.put("ORDER_ID", localData.getcurrentData().getOrderID());
            data.put("ORDER_STATUS", dataChange.toUpperCase());
            data.put("PHONE_NO", localData.getcurrentData().getDelPhoneNo());
            data.put("POST_CODE", localData.getcurrentData().getDeliPostCode());
            data.put("RIDER_ID", localData.getUserData().getId());
            data.put("TOTAL_PRICE", localData.getcurrentData().getOrderPrice());
            data.put("VERIFICATION_CODE", localData.getcurrentData().getVerificationCode());
            dataS = data.toString();
        }
        return dataS;
    }
}

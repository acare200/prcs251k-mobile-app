package com.example.tengfei.testconnection.backgroundClass;

import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.tengfei.testconnection.R;
import com.example.tengfei.testconnection.activity.Ava_Order_Detail;
import com.example.tengfei.testconnection.activity.MainMenu;
import com.example.tengfei.testconnection.activity.OrderHistory;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tengfei on 22/03/2017.
 */

public class Connection extends AsyncTask<String, String, String> {
    private Context context;
    private String type;
    private Button button;
    private ListView listView;
    private LocalData localData;
    private ArrayList<DeliDetail> deliDetailArrayList;
    private ArrayList<History> historyArrayList;
    private ArrayList<String> showList,showHisList;
    private UpdateServerData updateServerData;

    public Connection() { }

    public Connection(Context ctx, Button btn) {
        this.localData = new LocalData(ctx);
        this.context = ctx;
        this.button = btn;
    }

    public Connection(Context ctx, ListView listV) {
        this.context = ctx;
        this.listView = listV;
        this.localData = new LocalData(ctx);
        this.showList = new ArrayList<>();

    }

    public Connection(Context context) {
        this.context = context;
        this.localData = new LocalData(context);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    public Boolean checkConnection() {
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Service.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            if (networkInfo != null) {
                if (networkInfo.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    protected String doInBackground(String... params) {
        type = params[0];
        String username = null, password = null, checkMain = null;
        String dbURL, a, sJson = null;
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        InputStream inputStream = null;
        URL url;
        StringBuilder stringBuilder = new StringBuilder();
        GlobalData globalData = new GlobalData();
        dbURL = globalData.getUrl();
        showHisList =new ArrayList<>();
        deliDetailArrayList = new ArrayList<>();
        historyArrayList = new ArrayList<>();

        if (params.length == 3) {
            username = params[1];
            password = params[2];
        } else if (params.length == 2) {
            checkMain = params[1];
        }

        try {
            url = new URL(dbURL + type);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            inputStream = urlConnection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(inputStream));
            while ((a = reader.readLine()) != null) {
                stringBuilder.append(a);
            }
            sJson = stringBuilder.toString();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            stringBuilder.setLength(0);


        }

        try {
            if (type.contains("/")) {
                JSONObject jsonObject = new JSONObject(sJson);
                String status = jsonObject.getString("ORDER_STATUS");
                if (status.equals("READY")) {
                    return "cSuccess";
                } else {
                    return "cInvalid";
                }

            } else {
                JSONArray jsonArray = new JSONArray(sJson);
                int checkLength = jsonArray.length() - 1;
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject finalJson = jsonArray.getJSONObject(i);
                    if (type.equals("staff")) {
                        String checkType = finalJson.getString("STAFF_TYPE");
                        if (checkType.equals("RIDER")) {
                            String uEmail = finalJson.getString("EMAIL_USERNAME");
                            String uPassW = finalJson.getString("PASSWORD");
                            String salt = finalJson.getString("SALT");
                            String userP = getHash(salt+password);
                            if (username != null && username.equals(uEmail) && password != null &&
                                    userP.equals(uPassW)) {
                                int uId = finalJson.getInt("STAFF_ID");
                                String uFirstName = finalJson.getString("STAFF_NAME");
                                String uSurName = finalJson.getString("STAFF_SURNAME");
                                int uPhoneNo = finalJson.getInt("STAFF_CONTACT_NO");
                                int salaryRate = finalJson.getInt("SALARY_RATE");
                                int bankAcc = finalJson.getInt("STAFF_BANK_ACCOUNT");
                                User personDetail = new User(uEmail, uPassW, uId, uFirstName,
                                        uSurName, uPhoneNo, salaryRate, bankAcc, checkType,salt);
                                localData.setUserDetail(personDetail);
                                return "Success";
                            }
                        }
                        if (checkLength == i) {
                            return "Invalid";
                        }
                    }
                    if (type.equals("delivery")) {
                        if (checkMain != null) {
                            String storeAddress = globalData.getAddress() + " " + globalData.getPostCode();
                            String status = finalJson.getString("ORDER_STATUS");
                            String id = finalJson.getString("RIDER_ID");
                            int deliID = finalJson.getInt("DELIVERY_ID");
                            int orderID = finalJson.getInt("ORDER_ID");
                            int orderPrice = finalJson.getInt("TOTAL_PRICE");
                            int verificationCode = finalJson.getInt("VERIFICATION_CODE");
                            String dellAddress = finalJson.getString("ADDRESS");
                            String deliPc = finalJson.getString("POST_CODE");
                            String delName = finalJson.getString("CUSTOMER_NAME");
                            int phoneNo = finalJson.getInt("PHONE_NO");
                            String newline = System.getProperty("line.separator");
                            int distanceL = (int) getDistance(storeAddress, dellAddress + " " + deliPc);
                            String distance = String.valueOf(distanceL);

                            if (status.equals("READY") && checkMain.equals("Ava")) {
                                DeliDetail deliDetail = new DeliDetail(deliID, orderID, dellAddress,
                                        deliPc, delName, verificationCode, orderPrice, distanceL, phoneNo);
                                deliDetailArrayList.add(deliDetail);
                                if(showList!=null){
                                    showList.add("Delivery ID:" + deliID + "     Distance:" + distance +
                                            "m" + newline + "Address:" + dellAddress);
                                }else{
                                    return "dFail";
                                }

                            } else if (checkMain.equals("Main")) {
                                if (id.equals(String.valueOf(localData.getUserData().getId()))) {
                                    DeliDetail deliDetail = new DeliDetail(deliID, orderID, dellAddress,
                                            deliPc, delName, verificationCode, orderPrice, distanceL, phoneNo);
                                    localData.setcurrentDetail(deliDetail);
                                    return "mainSuc";
                                } else {
                                    return "mainNon";
                                }
                            }
                        }
                    }

                    if(type.equals("history")){
                        String checkID = finalJson.getString("RIDER_ID");
                        if( checkID !=  null)
                        {
                            if(checkID.equals(String.valueOf(localData.getUserData().getId()))){
                                int customerID = finalJson.getInt("CUSTOMER_ID");
                                int historyID = finalJson.getInt("ORDER_HISTORY_ID");
                                int riderID = finalJson.getInt("RIDER_ID");
                                String orderDate = finalJson.getString("ORDER_DATE");
                                String deliDate = finalJson.getString("DELIVERY_DATE_TIME");
                                String status = finalJson.getString("ORDER_STATUS");
                                History history = new History(customerID,historyID,riderID,orderDate,deliDate,status);
                                historyArrayList.add(history);
                                showHisList.add("History ID: " +historyID + "Status: " + status);
                            }

                        }

                    }
                }
                if (type.equals("delivery")) {
                    if (checkMain != null || checkMain.equals("Ava")) {
                        if (showList!=null){
                            return "dSucess";
                        }
                        else{
                            return "dFail";
                        }
                    }
                }
                if (type.equals("history")) {
                    if(showHisList.size()!=0){
                        return "historySucess";
                    }else{
                        return "historyFail";
                    }
                }


            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        if (type.equals("staff")) {
            button.setEnabled(true);
            switch (s) {
                case "Success":
                    Intent intent = new Intent(context, MainMenu.class);
                    context.startActivity(intent);
                    ((Activity) context).finish();
                    break;
                case "Invalid":
                    Toast.makeText(context, "Invalid username or password, try again.",
                            Toast.LENGTH_SHORT).show();
                    ((Activity) context).findViewById(R.id.progressBar).
                            setVisibility(View.INVISIBLE);
                    break;
            }
        }
        if (type.equals("delivery")) {
            switch (s) {
                case "dSucess":
                    if(showList.size()!= 0){
                        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context,
                                android.R.layout.simple_list_item_1, showList);
                        listView.setAdapter(arrayAdapter);
                        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                localData.setDetail(deliDetailArrayList.get(position));
                                Intent intent = new Intent(context, Ava_Order_Detail.class);
                                context.startActivity(intent);
                            }
                        });
                    }
                    break;
                case "dFail":
                    break;
                case "mainSuc":
                    break;
                case "mainNon":
                    break;

            }
        }
        if (type.contains("/")) {
            switch (s) {
                case "cSuccess":
                    updateServerData = new UpdateServerData(context);
                    updateServerData.execute("delivery", "ACCEPTED");
                    break;
                case "cInvalid":
                    localData = new LocalData(context);
                    localData.clearCurrentData();
                    Toast.makeText(context, "Sorry, this order is already been taken",
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }
        if (type.equals("history")) {
            if (s.equals("historySucess")) {
                if(showHisList.size()!=0){
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(context,
                            android.R.layout.simple_list_item_1, showHisList);
                    listView.setAdapter(arrayAdapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            localData.setHistoryDetail(historyArrayList.get(position));
                            Intent intent = new Intent(context, OrderHistory.class);
                            context.startActivity(intent);
                        }
                    });
                }

            }else if(s.equals("historyFail")){
                Toast.makeText(context,"You dont have any order completed",Toast.LENGTH_LONG).show();
            }
        }

    }

    public String getHash(String string) {
        String hashHex = "";
        byte byteData[];
        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        if (messageDigest != null) {
            messageDigest.update(string.getBytes());
            byteData = messageDigest.digest();
            StringBuilder stringBuilder = new StringBuilder();
            if (byteData != null) {
                for (int i = 0; i < byteData.length; i++) {
                    stringBuilder.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
                }
                hashHex = stringBuilder.toString();
            }
        }
        return hashHex;
    }

    private double getDistance(String sAdress, String eAdress) {
        Location location1 = getLocation(sAdress);
        Location location2 = getLocation(eAdress);
        return location1.distanceTo(location2);
    }

    public Location getLocation(String address) {
        List<Address> addressList = null;
        Geocoder geocoder = new Geocoder(context);
        try {
            addressList = geocoder.getFromLocationName(address, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (addressList != null) {
            Address adress = addressList.get(0);
            Location location = new Location("");
            location.setLatitude(adress.getLatitude());
            location.setLongitude(adress.getLongitude());
            return location;
        } else {
            return new Location(String.valueOf(new LatLng(0, 0)));
        }
    }
}

package com.example.tengfei.testconnection.backgroundClass;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Tengfei on 14/04/2017.
 */

public class LocalData {
    private static final String detail = "deliData";
    private static final String history = "history";
    private static final String current = "current";
    public static final String userData = "userData";
    SharedPreferences userLocalData;
    private SharedPreferences currentData;
    private SharedPreferences deliLocalData;
    private SharedPreferences hisLocalData;

    public LocalData(Context context) {
        deliLocalData = context.getSharedPreferences(detail, 0);
        hisLocalData = context.getSharedPreferences(history,0);
        currentData = context.getSharedPreferences(current, 0);
        userLocalData = context.getSharedPreferences(userData, 0);
    }

    void setDetail(DeliDetail deliDetail) {
        SharedPreferences.Editor dataEditor = deliLocalData.edit();
        dataEditor.putInt("did", deliDetail.getDeliID());
        dataEditor.putInt("id", deliDetail.getOrderID());
        dataEditor.putInt("price", deliDetail.getOrderPrice());
        dataEditor.putString("postcode", deliDetail.getDeliPostCode());
        dataEditor.putInt("verificationCode", deliDetail.getVerificationCode());
        dataEditor.putString("address", deliDetail.getDellAddress());
        dataEditor.putString("name", deliDetail.getDelName());
        dataEditor.putInt("phoneNo", deliDetail.getDelPhoneNo());
        dataEditor.putInt("distance", deliDetail.getDistance());
        dataEditor.apply();
    }

    public DeliDetail getData() {
        int deliID = deliLocalData.getInt("did", 0);
        int orderID = deliLocalData.getInt("id", 0);
        int orderPrice = deliLocalData.getInt("price", 0);
        String deliPc = deliLocalData.getString("postcode", "");
        int verificationCode = deliLocalData.getInt("verificationCode", 0);
        String dellAddress = deliLocalData.getString("address", "");
        String delName = deliLocalData.getString("name", "");
        int delPhoneNo = deliLocalData.getInt("phoneNo", 0);
        int distance = deliLocalData.getInt("distance", 0);
        return new DeliDetail(deliID, orderID, dellAddress, deliPc, delName, verificationCode, orderPrice, distance, delPhoneNo);
    }

    public void clearData() {
        SharedPreferences.Editor dataEditor;
        dataEditor = deliLocalData.edit();
        dataEditor.clear();

    }


    void setHistoryDetail(History history) {
        SharedPreferences.Editor historyEditor = hisLocalData.edit();
        historyEditor.putInt("customerID", history.getCustomerID());
        historyEditor.putInt("historyID", history.getHistoryID());
        historyEditor.putInt("riderID", history.getRiderID());
        historyEditor.putString("orderDate", history.getOrderDate());
        historyEditor.putString("deliDate", history.getDeliDate());
        historyEditor.putString("status", history.getStatus());
        historyEditor.apply();
    }

    public History getHistoryData() {
        int customerID = hisLocalData.getInt("customerID", 0);
        int historyID = hisLocalData.getInt("historyID", 0);
        int riderID = hisLocalData.getInt("riderID", 0);
        String orderDate = hisLocalData.getString("orderDate", "");
        String deliDate = hisLocalData.getString("deliDate", "");
        String status = hisLocalData.getString("status", "");

        return new History(customerID,historyID,riderID,orderDate,deliDate,status);
    }

    public void clearHistoryData() {
        SharedPreferences.Editor dataEditor;
        dataEditor = hisLocalData.edit();
        dataEditor.clear();

    }


    public void setcurrentDetail(DeliDetail deliDetail) {
        SharedPreferences.Editor currentDataEditor = currentData.edit();
        currentDataEditor.putInt("did", deliDetail.getDeliID());
        currentDataEditor.putInt("id", deliDetail.getOrderID());
        currentDataEditor.putInt("price", deliDetail.getOrderPrice());
        currentDataEditor.putString("postcode", deliDetail.getDeliPostCode());
        currentDataEditor.putInt("verificationCode", deliDetail.getVerificationCode());
        currentDataEditor.putString("address", deliDetail.getDellAddress());
        currentDataEditor.putString("name", deliDetail.getDelName());
        currentDataEditor.putInt("phoneNo", deliDetail.getDelPhoneNo());
        currentDataEditor.putInt("distance", deliDetail.getDistance());
        currentDataEditor.apply();
    }

    public DeliDetail getcurrentData() {
        int deliID = currentData.getInt("did", 0);
        int orderID = currentData.getInt("id", 0);
        int orderPrice = currentData.getInt("price", 0);
        String deliPc = currentData.getString("postcode", "");
        int verificationCode = currentData.getInt("verificationCode", 0);
        String dellAddress = currentData.getString("address", "");
        String delName = currentData.getString("name", "");
        int delPhoneNo = currentData.getInt("phoneNo", 0);
        int distance = currentData.getInt("distance", 0);
        return new DeliDetail(deliID, orderID, dellAddress, deliPc, delName, verificationCode, orderPrice, distance, delPhoneNo);
    }

    public void clearCurrentData() {
        SharedPreferences.Editor dataEditor = currentData.edit();
        dataEditor.clear();

    }

    void setUserDetail(User user) {
        SharedPreferences.Editor userEditor = userLocalData.edit();
        userEditor.putString("email", user.getEmail());
        userEditor.putString("password", user.getPassword());
        userEditor.putInt("id", user.getId());
        userEditor.putString("firstname", user.getFirstName());
        userEditor.putString("surname", user.getSurName());
        userEditor.putInt("phonenumber", user.getPhoneNo());
        userEditor.putInt("salaryRate", user.getSalaryRate());
        userEditor.putInt("bankAcc", user.getBankAcc());
        userEditor.putString("type", user.getType());
        userEditor.putString("salt",user.getSalt());
        userEditor.apply();
    }

    public User getUserData() {
        String email = userLocalData.getString("email", "");
        String password = userLocalData.getString("password", "");
        int id = userLocalData.getInt("id", 0);
        String firstname = userLocalData.getString("firstname", "");
        String surname = userLocalData.getString("surname", "");
        int phonenumber = userLocalData.getInt("phonenumber", 0);
        int salaryRate = userLocalData.getInt("salaryRate", 0);
        int bankAcc = userLocalData.getInt("backAcc", 0);
        String type = userLocalData.getString("type", "");
        String salt = userLocalData.getString("salt","");
        return new User(email, password, id, firstname, surname, phonenumber, salaryRate, bankAcc, type,salt);
    }

    public void clearUserData() {
        SharedPreferences.Editor dataEditor = userLocalData.edit();
        dataEditor.clear();

    }
}

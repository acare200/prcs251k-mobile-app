package com.example.tengfei.testconnection.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import com.example.tengfei.testconnection.R;
import com.example.tengfei.testconnection.backgroundClass.LocalData;

public class History_Detail extends AppCompatActivity {

    private TextView txtCID, txtRID, txtHID, txtODATE, txtDDATE, txtStatus;
    LocalData localData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history__detail);
        localData = new LocalData(this);
        txtCID = (TextView) findViewById(R.id.customerID);
        txtRID = (TextView)findViewById(R.id.riderID);
        txtHID = (TextView)findViewById(R.id.historyID);
        txtODATE = (TextView)findViewById(R.id.orderDate);
        txtDDATE = (TextView)findViewById(R.id.deliverDate);
        txtStatus = (TextView)findViewById(R.id.orderStatus);

        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        setInfo();
    }

    private void setInfo() {
        txtCID.setText(String.valueOf(localData.getHistoryData().getCustomerID()));
        txtRID.setText(String.valueOf(localData.getHistoryData().getRiderID()));
        txtHID.setText(String.valueOf(localData.getHistoryData().getHistoryID()));
        txtODATE.setText(String.valueOf(localData.getHistoryData().getOrderDate()));
        txtDDATE.setText(String.valueOf(localData.getHistoryData().getDeliDate()));
        txtStatus.setText(String.valueOf(localData.getHistoryData().getStatus()));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home)
        {
            localData.clearHistoryData();
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

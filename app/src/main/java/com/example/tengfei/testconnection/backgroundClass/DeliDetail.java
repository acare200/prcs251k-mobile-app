package com.example.tengfei.testconnection.backgroundClass;

/**
 * Created by Tengfei on 13/04/2017.
 */

public class DeliDetail {
    int delPhoneNo;
    private int deliID;
    private int orderID;
    private int orderPrice;
    private String deliPostCode = "";
    private int verificationCode;
    private String dellAddress = "";
    private String delName = "";
    private int distance;


    DeliDetail(int did, int oid, String address, String deliPostC,
               String name, int code, int price, int distance, int phoneN) {
        this.deliID = did;
        this.orderID = oid;
        this.orderPrice = price;
        this.deliPostCode = deliPostC;
        this.verificationCode = code;
        this.dellAddress = address;
        this.delName = name;
        this.distance = distance;
        this.delPhoneNo = phoneN;


    }

    public int getDeliID() {
        return deliID;
    }

    public int getOrderID() {
        return orderID;
    }

    public int getOrderPrice() {
        return orderPrice;
    }

    public String getDeliPostCode() {
        return deliPostCode;
    }

    public int getVerificationCode() {
        return verificationCode;
    }

    public String getDellAddress() {
        return dellAddress;
    }

    public String getDelName() {
        return delName;
    }

    public int getDistance() {
        return distance;
    }

    public int getDelPhoneNo() {
        return delPhoneNo;
    }
}

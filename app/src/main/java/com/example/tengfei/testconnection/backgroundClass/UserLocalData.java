package com.example.tengfei.testconnection.backgroundClass;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Tengfei on 03/04/2017.
 */

public class UserLocalData {
    public static final String detail = "userData";
    SharedPreferences userLocalData;

    public UserLocalData(Context context) {
        userLocalData = context.getSharedPreferences(detail, 0);
    }

    void setDetail(User user) {
        SharedPreferences.Editor dataEditor = userLocalData.edit();
        dataEditor.putString("email", user.getEmail());
        dataEditor.putString("password", user.getPassword());
        dataEditor.putInt("id", user.getId());
        dataEditor.putString("firstname", user.getFirstName());
        dataEditor.putString("surname", user.getSurName());
        dataEditor.putInt("phonenumber", user.getPhoneNo());
        dataEditor.putInt("salaryRate", user.getSalaryRate());
        dataEditor.putInt("bankAcc", user.getBankAcc());
        dataEditor.putString("type", user.getType());
        dataEditor.apply();
    }

    public User getData() {
        String email = userLocalData.getString("email", "");
        String password = userLocalData.getString("password", "");
        int id = userLocalData.getInt("id", 0);
        String firstname = userLocalData.getString("firstname", "");
        String surname = userLocalData.getString("surname", "");
        int phonenumber = userLocalData.getInt("phonenumber", 0);
        int salaryRate = userLocalData.getInt("salaryRate", 0);
        int bankAcc = userLocalData.getInt("backAcc", 0);
        String type = userLocalData.getString("type", "");
        String salt = userLocalData.getString("","");
        User newuser = new User(email, password, id, firstname, surname, phonenumber, salaryRate, bankAcc, type,salt);
        return newuser;
    }

    public void clearData() {
        SharedPreferences.Editor dataEditor = userLocalData.edit();
        dataEditor.clear();

    }
}

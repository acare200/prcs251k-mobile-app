package com.example.tengfei.testconnection.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tengfei.testconnection.R;
import com.example.tengfei.testconnection.backgroundClass.Connection;

public class Login extends AppCompatActivity {

    private TextView userName;
    private TextView pass;
    public Button button;
    public ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        userName = (TextView)findViewById(R.id.txtUser);
        pass = (TextView)findViewById(R.id.txtPass);
        button = (Button)findViewById(R.id.btnClick);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);

        userName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(userName.getText().toString().isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(userName.getText().toString()).matches())
                {
                    userName.setError("Email invalid or empty");
                }
            }
        });
        pass.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(pass.getText().toString().isEmpty())
                {
                   pass.setError("Password cannot be empty");
                }
            }
        });
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = userName.getText().toString();
                String password = pass.getText().toString();
                if(!username.isEmpty() && !password.isEmpty())
                {
                    progressBar.setVisibility(View.VISIBLE);
                    button.setEnabled(false);
                    String type = "staff";
                    Connection connection = new Connection(Login.this,button);
                    if(connection.checkConnection())
                    {
                        connection.execute(type,username,password);
                    }else
                    {
                        button.setEnabled(true);
                        Toast.makeText(Login.this,"Oops, Connection Lost, Please try later!!",Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.INVISIBLE);
                    }

                }
            }
        });
    }



}

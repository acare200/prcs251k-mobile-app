package com.example.tengfei.testconnection.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;

import com.example.tengfei.testconnection.R;
import com.example.tengfei.testconnection.backgroundClass.Connection;
import com.example.tengfei.testconnection.backgroundClass.LocalData;

public class OrderHistory extends AppCompatActivity {
    private ListView listView;
    Connection connection;
    LocalData history;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);
        listView = (ListView)findViewById(R.id.avaList);
        history = new LocalData(this);
    }

    private void activityInitial(){
        if(history.getHistoryData()!= null)
        {
            history.clearData();
        }
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        connection = new Connection(OrderHistory.this, listView);
        connection.execute("history");
    }

    @Override
    protected void onResume() {
        activityInitial();
        super.onResume();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home)
        {
            if(history.getData()!= null){
                history.clearData();
            }
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

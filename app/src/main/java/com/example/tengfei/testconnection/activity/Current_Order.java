package com.example.tengfei.testconnection.activity;

import android.content.Intent;
import android.os.LocaleList;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.tengfei.testconnection.R;
import com.example.tengfei.testconnection.backgroundClass.CurrentOrderLocalData;
import com.example.tengfei.testconnection.backgroundClass.LocalData;
import com.example.tengfei.testconnection.backgroundClass.UpdateServerData;

public class Current_Order extends AppCompatActivity {
    private TextView did, oid, address, postCode, name, phoneN;
    private Button backStore, startDeli, collect, cancel, verify;
    private EditText verifyCode;
    UpdateServerData updateServerData;
    LocalData localData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current__order);
        localData = new LocalData(this);

        verify =(Button)findViewById(R.id.btnVerify);
        verifyCode =(EditText)findViewById(R.id.txtCode);
        did = (TextView)findViewById(R.id.deliveryID);
        oid = (TextView)findViewById(R.id.orderID);
        address = (TextView)findViewById(R.id.deliveryAddress);
        postCode = (TextView)findViewById(R.id.deliveryPc);
        name = (TextView)findViewById(R.id.deliveryName);
        phoneN= (TextView)findViewById(R.id.deliveryPhone);
        backStore = (Button)findViewById(R.id.storeNav);
        startDeli = (Button)findViewById(R.id.deliverNav);
        collect = (Button)findViewById(R.id.orderCollect);
        cancel = (Button)findViewById(R.id.orderCancel);
    }

    @Override
    protected void onResume() {
            setInfo();
            setOnclick();
        super.onResume();
    }

    private void setOnclick() {
        backStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Current_Order.this, MapsActivity.class);
                intent.putExtra("type","store");
                Current_Order.this.startActivity(intent);
            }
        });

        startDeli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Current_Order.this, MapsActivity.class);
                intent.putExtra("type","delivery");
                Current_Order.this.startActivity(intent);

            }
        });

        collect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateServerData = new UpdateServerData(Current_Order.this);
                updateServerData.execute("delivery", "COLLECTED");
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateServerData = new UpdateServerData(Current_Order.this);
                updateServerData.execute("delivery", "READY");


            }
        });
        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(verifyCode.getText()!= null)
                {
                    String i =verifyCode.getText().toString();
                    if(validate(i)){
                        updateServerData = new UpdateServerData(Current_Order.this);
                        updateServerData.execute("delivery", "DELIVERED");
                    }else{
                        Toast.makeText(Current_Order.this,"Invalid code",Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(Current_Order.this,"Invalid code",Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    private boolean validate(String text) {
        if(text.equals(String.valueOf(localData.getcurrentData().getVerificationCode()))){
            return true;
        }
        return false;
    }

    private void setInfo() {
        did.setText(String.valueOf(localData.getcurrentData().getDeliID()));
        oid.setText(String.valueOf(localData.getcurrentData().getOrderID()));
        address.setText(localData.getcurrentData().getDellAddress());
        postCode.setText(localData.getcurrentData().getDeliPostCode());
        name.setText(localData.getcurrentData().getDelName());
        phoneN.setText(String.valueOf(localData.getcurrentData().getDelPhoneNo()));
    }
}

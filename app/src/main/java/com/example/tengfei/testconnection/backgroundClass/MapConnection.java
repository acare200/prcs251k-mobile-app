package com.example.tengfei.testconnection.backgroundClass;

import android.content.Context;
import android.graphics.Color;
import android.os.AsyncTask;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tengfei on 28/04/2017.
 */

public class MapConnection extends AsyncTask<String, String, String> {
    private GoogleMap googleMap;
    private Context context;

    public MapConnection(Context context, GoogleMap googleMap) {
        this.context = context;
        this.googleMap = googleMap;
    }

    @Override
    protected String doInBackground(String... params) {

        String urlS = "";
        HttpURLConnection urlConnection;
        BufferedReader reader;
        String a;
        URL url;
        StringBuffer buffer = new StringBuffer();
        String returnV = "";
        if (params != null) {
            String[] position = params[0].split(",");
            String[] position2 = params[1].split(",");
            LatLng startPoint = new LatLng(Double.parseDouble(position[0]), Double.parseDouble(position[1]));
            LatLng endPoint = new LatLng(Double.parseDouble(position2[0]), Double.parseDouble(position2[1]));
            urlS = urlString(startPoint, endPoint);
        }

        try {
            url = new URL(urlS);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            InputStream inputStream = urlConnection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(inputStream));

            while ((a = reader.readLine()) != null) {
                buffer.append(a);
            }
            returnV = buffer.toString();
            urlConnection.disconnect();
            reader.close();
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return returnV;
    }

    private String urlString(LatLng latLngS, LatLng latLngE) {
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/directions/json");
        urlString.append("?origin=");// from
        urlString.append(Double.toString(latLngS.latitude));
        urlString.append(",");
        urlString
                .append(Double.toString(latLngS.longitude));
        urlString.append("&destination=");// to
        urlString
                .append(Double.toString(latLngE.latitude));
        urlString.append(",");
        urlString.append(Double.toString(latLngE.longitude));
        urlString.append("&sensor=true&mode=driving&alternatives=true");
        String url = urlString.toString();
        return url;
    }

    @Override
    protected void onPostExecute(String s) {
        drawPath(s);
        super.onPostExecute(s);
    }


    public void drawPath(String result) {

        try {
            //Tranform the string into a json object
            final JSONObject json = new JSONObject(result);
            JSONArray routeArray = json.getJSONArray("routes");
            JSONObject routes = routeArray.getJSONObject(0);
            JSONObject overviewPolylines = routes.getJSONObject("overview_polyline");
            String encodedString = overviewPolylines.getString("points");
            List<LatLng> list = decodePoly(encodedString);
            googleMap.addPolyline(new PolylineOptions()
                    .addAll(list)
                    .width(12)
                    .color(Color.parseColor("#05b1fb"))//Google maps blue color
                    .geodesic(true)
            );

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private List<LatLng> decodePoly(String encoded) {

        List<LatLng> polyLatLng = new ArrayList<>();
        int index = 0, length = encoded.length();
        int lat = 0, lng = 0;

        while (index < length) {
            int b, shift = 0, result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = encoded.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            LatLng p = new LatLng((((double) lat / 1E5)),
                    (((double) lng / 1E5)));
            polyLatLng.add(p);
        }
        return polyLatLng;
    }

}

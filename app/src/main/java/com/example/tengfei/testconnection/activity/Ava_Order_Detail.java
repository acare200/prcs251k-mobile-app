package com.example.tengfei.testconnection.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.tengfei.testconnection.R;
import com.example.tengfei.testconnection.backgroundClass.Connection;
import com.example.tengfei.testconnection.backgroundClass.CurrentOrderLocalData;
import com.example.tengfei.testconnection.backgroundClass.LocalData;
import com.example.tengfei.testconnection.backgroundClass.UpdateServerData;

public class Ava_Order_Detail extends AppCompatActivity {

    Connection connection;
    UpdateServerData updateServerData;
    LocalData localData;
    private Button btnConfirm;
    private TextView did, oid, address, postCode, phoneN, name, distance, price;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ava__order__detail);
        did = (TextView)findViewById(R.id.txtDid);
        oid = (TextView)findViewById(R.id.txtOid);
        address = (TextView)findViewById(R.id.txtAddress);
        phoneN= (TextView)findViewById(R.id.txtPhoneNo);
        postCode= (TextView)findViewById(R.id.txtPostCode);
        name= (TextView)findViewById(R.id.txtName);
        distance = (TextView)findViewById(R.id.txtDistance);
        price = (TextView)findViewById(R.id.txtPrice);
        btnConfirm = (Button)findViewById(R.id.btnAccept);
        localData = new LocalData(this);


        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        setInfo();

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               alertBox();
            }
        });
    }

    private void setInfo() {
        did.setText(String.valueOf(localData.getData().getDeliID()));
        oid.setText(String.valueOf(localData.getData().getOrderID()));
        address.setText(localData.getData().getDellAddress());
        postCode.setText(localData.getData().getDeliPostCode());
        name.setText(localData.getData().getDelName());
        price.setText(String.valueOf(localData.getData().getOrderPrice()));
        phoneN.setText(String.valueOf(localData.getData().getDelPhoneNo()));
        distance.setText(String.valueOf(localData.getData().getDistance()) + " M");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home)
        {
            localData.clearData();
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void alertBox()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want accept this order?")
                .setCancelable(true)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        localData.setcurrentDetail(localData.getData());
                        connection =new Connection(Ava_Order_Detail.this);
                        String string = "delivery/" + localData.getData().getDeliID();
                        connection.execute(string);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        builder.create().show();
    }
}

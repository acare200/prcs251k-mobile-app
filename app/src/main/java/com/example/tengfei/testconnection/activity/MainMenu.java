package com.example.tengfei.testconnection.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.tengfei.testconnection.R;
import com.example.tengfei.testconnection.backgroundClass.Connection;
import com.example.tengfei.testconnection.backgroundClass.CurrentOrderLocalData;
import com.example.tengfei.testconnection.backgroundClass.History;
import com.example.tengfei.testconnection.backgroundClass.LocalData;
import com.example.tengfei.testconnection.backgroundClass.UserLocalData;

public class MainMenu extends AppCompatActivity {
    LocalData localData;
    Connection connection;

    private Button ava_order, cur_order, p_detail, his_order, store_map, log_out;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        ava_order = (Button)findViewById(R.id.avaO);
        cur_order = (Button)findViewById(R.id.curO);
        p_detail = (Button)findViewById(R.id.pDetail);
        his_order  = (Button)findViewById(R.id.orderH);
        store_map = (Button)findViewById(R.id.storeM);
        log_out = (Button)findViewById(R.id.logOut);


        localData = new LocalData(this);

    }

    @Override
    protected void onStart() {
        buttonOnclick();
        super.onStart();
    }

    @Override
    protected void onResume() {
        buttonOnclick();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void buttonOnclick()
    {
        validateButton();
        ava_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMenu.this, AvaOrder.class);
                startActivity(intent);
            }
        });
        cur_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(localData.getcurrentData()!=null){
                    Intent intent = new Intent(MainMenu.this, Current_Order.class);
                    MainMenu.this.startActivity(intent);
                }else{
                    Toast.makeText(MainMenu.this,"You havent accept any order yet!",Toast.LENGTH_LONG).show();
                }

            }
        });
        p_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMenu.this, PersonalData.class);
                startActivity(intent);
            }
        });
        his_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMenu.this, OrderHistory.class);
                MainMenu.this.startActivity(intent);
            }
        });
        store_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMenu.this, MapsActivity.class);
                intent.putExtra("type","store");
                MainMenu.this.startActivity(intent);

            }
        });
        log_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                localData.clearData();
                localData.clearCurrentData();
                localData.clearUserData();
                localData.clearHistoryData();
                Intent intent = new Intent(MainMenu.this,Login.class);
                MainMenu.this.startActivity(intent);
                MainMenu.this.finish();
            }
        });
    }

    private void validateButton() {
        connection = new Connection(MainMenu.this);
        connection.execute("delivery","Main");
    }
}

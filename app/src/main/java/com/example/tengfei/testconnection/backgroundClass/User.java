package com.example.tengfei.testconnection.backgroundClass;

import java.io.Serializable;

/**
 * Created by Tengfei on 27/03/2017.
 */

public class User implements Serializable {
    private String email = null,password = null, firstName = null,
            surName = null, type = null,salt = null;
    private int id, phoneNo, salaryRate, bankAcc;

    User(String uEmail, String uPassW, int uId, String uFirstName, String uSurName,
         int uPhoneNo, int salaryRate, int bankAcc, String type, String salt) {
        this.email = uEmail;
        this.password = uPassW;
        this.id = uId;
        this.firstName = uFirstName;
        this.surName = uSurName;
        this.phoneNo = uPhoneNo;
        this.salaryRate = salaryRate;
        this.bankAcc = bankAcc;
        this.type = type;
        this.salt = salt;
    }

    public String getSalt() {
        return salt;
    }

    String getType() {
        return type;
    }

    int getSalaryRate() {
        return salaryRate;
    }

    int getBankAcc() {
        return bankAcc;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSurName() {
        return surName;
    }

    public int getPhoneNo() {
        return phoneNo;
    }

}

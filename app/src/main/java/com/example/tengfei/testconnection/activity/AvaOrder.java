package com.example.tengfei.testconnection.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;

import com.example.tengfei.testconnection.R;
import com.example.tengfei.testconnection.backgroundClass.Connection;
import com.example.tengfei.testconnection.backgroundClass.LocalData;

public class AvaOrder extends AppCompatActivity {

    private ListView listView;
    Connection connection;
    LocalData deliDetail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ava_order);
        listView = (ListView)findViewById(R.id.avaList);
        deliDetail = new LocalData(this);

    }



    private void activityInitial(){
        if(deliDetail.getData()!= null)
        {
            deliDetail.clearData();
        }
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        connection = new Connection(AvaOrder.this, listView);
        connection.execute("delivery","Ava");
    }

    @Override
    protected void onResume() {
        activityInitial();
        super.onResume();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home)
        {
            if(deliDetail.getData()!= null){
                deliDetail.clearData();
            }
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

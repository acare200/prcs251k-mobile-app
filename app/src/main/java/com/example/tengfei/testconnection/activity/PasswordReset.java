package com.example.tengfei.testconnection.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.tengfei.testconnection.R;
import com.example.tengfei.testconnection.backgroundClass.Connection;
import com.example.tengfei.testconnection.backgroundClass.LocalData;
import com.example.tengfei.testconnection.backgroundClass.UpdateServerData;

public class PasswordReset extends AppCompatActivity {

    EditText oldPass, newPass, newPass2;
    private Button btn;
    Connection connection;
    LocalData localData;
    UpdateServerData updateServerData;
    String newHashPass = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_reset);
        oldPass = (EditText)findViewById(R.id.txtOldPass);
        newPass = (EditText)findViewById(R.id.txtNewPass);
        newPass2 = (EditText)findViewById(R.id.txtCFNewPass);
        btn = (Button)findViewById(R.id.btnPassChange);
        connection = new Connection();
        localData = new LocalData(this);
        updateServerData = new UpdateServerData(this);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validation(oldPass.getText().toString(), newPass.getText().toString(), newPass2.getText().toString())){
                    alertBox();
                }else{
                    Toast.makeText(PasswordReset.this,"Invalid Password, or the new password not matched.",Toast.LENGTH_SHORT).show();
                }

            }
        });


    }

    private Boolean validation(String oldPass, String newPass, String newPass2)
    {
        String i = localData.getUserData().getSalt();
        String total = connection.getHash(i+oldPass);
        //validation
        if(total.equals(localData.getUserData().getPassword())){
            if(newPass.equals(newPass2)){
                newHashPass = connection.getHash(i + newPass);
                return true;
            }else {
                return false;
            }
        }
        return false;
    }

    private void alertBox()
    {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you wish to change the password?")
                .setCancelable(true)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        updateServerData = new UpdateServerData(PasswordReset.this);
                        updateServerData.execute("staff", newHashPass);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();
                    }
                });
        builder.create().show();
    }
}

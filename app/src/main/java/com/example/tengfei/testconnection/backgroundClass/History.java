package com.example.tengfei.testconnection.backgroundClass;

/**
 * Created by Tengfei on 11/05/2017.
 */

public class History {
    private int customerID, historyID, riderID;
    private String orderDate, deliDate, status;

    public History(int customerID, int historyID, int riderID, String orderDate,
                   String deliDate, String status){
        this.customerID = customerID;
        this.historyID = historyID;
        this.riderID = riderID;
        this.orderDate = orderDate;
        this.deliDate = deliDate;
        this.status = status;
    }

    public int getCustomerID() {
        return customerID;
    }

    public int getHistoryID() {
        return historyID;
    }

    public int getRiderID() {
        return riderID;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public String getDeliDate() {
        return deliDate;
    }

    public String getStatus() {
        return status;
    }
}

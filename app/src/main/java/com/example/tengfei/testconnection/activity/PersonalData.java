package com.example.tengfei.testconnection.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.tengfei.testconnection.R;
import com.example.tengfei.testconnection.backgroundClass.LocalData;
import com.example.tengfei.testconnection.backgroundClass.UserLocalData;

public class PersonalData extends AppCompatActivity {

    private TextView email, id, firstName, lastName, phoneNo;
    private Button btn;
    LocalData localData;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_data);

        email = (TextView)findViewById(R.id.userEmail);
        id = (TextView)findViewById(R.id.userID);
        firstName = (TextView)findViewById(R.id.userFName);
        lastName = (TextView)findViewById(R.id.userSName);
        phoneNo = (TextView)findViewById(R.id.userPhoneNo);
        btn = (Button)findViewById(R.id.btnChangePass);
        localData = new LocalData(this);

        if(getSupportActionBar() != null)
        {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


        GetInfo();


    }

    private void GetInfo() {
        email.setText(localData.getUserData().getEmail());
        id.setText(String.valueOf(localData.getUserData().getId()));
        firstName.setText(localData.getUserData().getFirstName());
        lastName.setText(localData.getUserData().getSurName());
        phoneNo.setText(String.valueOf(localData.getUserData().getPhoneNo()));
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PersonalData.this, PasswordReset.class);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home)
        {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

package com.example.tengfei.testconnection.backgroundClass;

/**
 * Created by Tengfei on 01/05/2017.
 */

public class GlobalData {
    private final String address = "Plymouth University";
    private final String postCode = "PL4 8AA";
    private final String url = "http://xserve.uopnet.plymouth.ac.uk/modules/INTPROJ/PRCS251K/api/";

    public String getUrl() {
        return url;
    }

    public String getAddress() {
        return address;
    }

    public String getPostCode() {
        return postCode;
    }
}

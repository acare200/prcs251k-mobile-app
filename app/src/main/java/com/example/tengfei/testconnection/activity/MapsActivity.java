package com.example.tengfei.testconnection.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.tengfei.testconnection.R;
import com.example.tengfei.testconnection.backgroundClass.Connection;
import com.example.tengfei.testconnection.backgroundClass.CurrentOrderLocalData;
import com.example.tengfei.testconnection.backgroundClass.GlobalData;
import com.example.tengfei.testconnection.backgroundClass.LocalData;
import com.example.tengfei.testconnection.backgroundClass.MapConnection;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private static final int ACCESS_FINE_LOCATION = 101;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATE = 100;
    private static final long MIN_TME_BW_UPDATES = 1000 * 10;
    private LocationManager locationManager;
    private GlobalData globalData;
    private String type = "";
    private LatLng startPoint;
    private LatLng endPoint;
    MapConnection mapConnection;
    private Button startNav, cancelNav;
    private LocalData currentOrderLocalData;

    private final LocationListener gpsLocationListener = new LocationListener() {

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            switch (status) {
                case LocationProvider.AVAILABLE:
                    Toast.makeText(MapsActivity.this, "Available",
                            Toast.LENGTH_SHORT).show();
                    break;
                case LocationProvider.OUT_OF_SERVICE:
                    Toast.makeText(MapsActivity.this, "GPS out of service",
                            Toast.LENGTH_SHORT).show();
                    break;
                case LocationProvider.TEMPORARILY_UNAVAILABLE:
                    Toast.makeText(MapsActivity.this, "GPS temporarily unavailable",
                            Toast.LENGTH_SHORT).show();
                    break;
            }
        }

        @Override
        public void onProviderEnabled(String provider) {
            Toast.makeText(MapsActivity.this, provider.toUpperCase() +" Enabled",
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onProviderDisabled(String provider) {
            Toast.makeText(MapsActivity.this, provider.toUpperCase() +" Disabled",
                    Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onLocationChanged(Location location) {
            startPoint = null;
            endPoint = null;
            locationManager.removeUpdates(networkLocationListener);
            startPoint = new LatLng(location.getLatitude(), location.getLongitude());
            Toast.makeText(MapsActivity.this, "new GPS" + startPoint.toString(),
                    Toast.LENGTH_LONG).show();
            postNav();
        }
    };
    private final LocationListener networkLocationListener =
            new LocationListener() {
                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {
                    switch (status) {
                        case LocationProvider.AVAILABLE:
                            Toast.makeText(MapsActivity.this, "Network location available " +
                                    "again", Toast.LENGTH_SHORT).show();
                            break;
                        case LocationProvider.OUT_OF_SERVICE:
                            Toast.makeText(MapsActivity.this, "Network location out of" +
                                    " service\n", Toast.LENGTH_SHORT).show();
                            break;
                        case LocationProvider.TEMPORARILY_UNAVAILABLE:
                            Toast.makeText(MapsActivity.this, "Network location temporarily" +
                                    " unavailable\n", Toast.LENGTH_SHORT).show();
                            break;
                    }
                }

                @Override
                public void onProviderEnabled(String provider) {
                    Toast.makeText(MapsActivity.this, provider.toUpperCase() +" Enabled\n",
                            Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onProviderDisabled(String provider) {
                    Toast.makeText(MapsActivity.this, provider.toUpperCase() +" Disabled\n",
                            Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onLocationChanged(Location location) {
                    startPoint = null;
                    endPoint = null;
                    startPoint = new LatLng(location.getLatitude(), location.getLongitude());
                    Toast.makeText(MapsActivity.this, "new network" + startPoint.toString(),
                            Toast.LENGTH_LONG).show();
                    postNav();
                }
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        globalData = new GlobalData();
        startNav = (Button)findViewById(R.id.startNav);
        cancelNav = (Button)findViewById(R.id.cancelNav);

        Intent intent = getIntent();
        type = intent.getStringExtra("type");
        if(locationManager!=null){
            locationManager = (LocationManager)
                    getSystemService(Context.LOCATION_SERVICE);
        }


        currentOrderLocalData = new LocalData(this);
        setButton();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        }
    }

    private void setButton()
    {
        startNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startUpdate();
            }
        });

        cancelNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationManager.removeUpdates(networkLocationListener);
                locationManager.removeUpdates(gpsLocationListener);
            }
        });
    }



    private void postNav(){
        if (type.equals("store")) {
            String destination = globalData.getAddress() + globalData.getPostCode();
            List<Address> addressList = null;
            Geocoder geocoder = new Geocoder(this);
            try {
                addressList = geocoder.getFromLocationName(destination, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (addressList != null) {
                Address adress = addressList.get(0);
                endPoint = new LatLng(adress.getLatitude(),adress.getLongitude());
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(endPoint).title("Destination"));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(startPoint, 18));
                mMap.addMarker(new MarkerOptions().position(endPoint).title("Destination"));
                startNav();
            }
        } else if (type == "delivery") {
            String destination = currentOrderLocalData.getcurrentData().getDellAddress()
                    + currentOrderLocalData.getcurrentData().getDeliPostCode();
            List<Address> addressList = null;
            Geocoder geocoder = new Geocoder(this);
            try {
                addressList = geocoder.getFromLocationName(destination, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (addressList != null) {
                Address adress = addressList.get(0);
                endPoint = new LatLng(adress.getLatitude(),adress.getLongitude());
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(endPoint).title("Destination"));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(startPoint, 18));
                mMap.addMarker(new MarkerOptions().position(endPoint).title("Destination"));
                startNav();
            }
        }
    }

    private void startNav(){
        String start = startPoint.latitude + "," + startPoint.longitude;
        String end = endPoint.latitude + "," + endPoint.longitude;
        mapConnection = new MapConnection(this,mMap);
        mapConnection.execute(start,end);

    }
    private void removeUpdate(){
        locationManager.removeUpdates(networkLocationListener);
        locationManager.removeUpdates(gpsLocationListener);
    }

    private void startUpdate()
    {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            if(locationManager!=null){
                locationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER, MIN_TME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATE, networkLocationListener);
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                        MIN_TME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATE, gpsLocationListener);
            }
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
       startUpdate();
    }

    @Override
    protected void onPause(){
        super.onPause();
        removeUpdate();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults){
        if(requestCode == ACCESS_FINE_LOCATION)
        {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                if(ActivityCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED){
                    onResume();
                }
            }
        }else{
            Toast.makeText(this,"permission denied",Toast.LENGTH_LONG).show();
            this.finish();
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}



